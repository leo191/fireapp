from auth_mon import init
from firebase_admin import db
import datetime
import time

wait_time = 5
init()
try:
    ref = db.reference('/PROFILE')

    mon_ref = ref.child('timest')
    mon_ref.set({'time': str(datetime.datetime.now())})

    time.sleep(wait_time)

    result = mon_ref.get('/PROFILE')
    at = datetime.datetime.strptime(result[0]['time'], '%Y-%m-%d %H:%M:%S.%f')

    now = datetime.datetime.now()
    # at = datetime.datetime.strptime(push_data,'%Y-%m-%dT%H:%M:%S.%f')
    # now = datetime.datetime.now()
    if now > at:
        print("delay of %d seconds"%((now - at).seconds))
except Exception as e:
    print(e)