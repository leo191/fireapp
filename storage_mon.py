
from init import init_fire
import filecmp
from firebase_admin import storage

init_fire()

def upload_blob(source_file_name, destination_blob_name):
    bucket = storage.bucket()
  
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print('File {} uploaded to {}.'.format(
        source_file_name,
        destination_blob_name))


def download_blob(source_blob_name, destination_file_name):
    bucket = storage.bucket()
    blob = bucket.blob(source_blob_name)

    blob.download_to_filename(destination_file_name)

    print('Blob {} downloaded to {}.'.format(
        source_blob_name,
        destination_file_name))




def storage_check():
    upload_blob('tt.txt','uu.txt')
    download_blob('uu.txt', 'dd.txt')
    if filecmp.cmp('tt.txt', 'dd.txt'):
        print('storage success')
    else:
        print('storage failed')    

storage_check()